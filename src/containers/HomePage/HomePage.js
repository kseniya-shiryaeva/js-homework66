import React, {useCallback, useEffect, useState} from 'react';
import './HomePage.css';
import axiosApi from "../../axiosApi";
import Quote from "../../components/Quote/Quote";
import SidebarMenu from "../../components/SidabarMenu/SidebarMenu";
import loadHandler from "../../hoc/loadHandler";

const HomePage = ({match}) => {
    const [quotes, setQuotes] = useState(null);

    const fetchData = useCallback(async () => {
        let quotesUrl = '/quotes.json';
        if (match.params.category) {
            quotesUrl += '?orderBy="category"&equalTo="' + match.params.category + '"';
        }
        const request = await axiosApi.get(quotesUrl);
        setQuotes(request.data);
    }, [match.params.category]);

    useEffect(() => {
        fetchData().catch(console.error);
    }, [match.params.category]);

    const RemoveQuote = async id => {
        try {
            await axiosApi.delete('quotes/' + id + '.json');
        } finally {
            fetchData().catch(console.error);
        }
    };

    let content = 'Цитат пока нет';

    if (quotes) {
        content = Object.keys(quotes).map(key => {
                return <Quote key={key} text={quotes[key].text} id={key} author={quotes[key].author} removeQuote={() => RemoveQuote(key)} />
            });
    }

    return (
        <div className="HomePage">
            <div className="col-1">
                <SidebarMenu />
            </div>
            <div className="col-2">
                {content}
            </div>
        </div>
    );
};

export default loadHandler(HomePage, axiosApi);
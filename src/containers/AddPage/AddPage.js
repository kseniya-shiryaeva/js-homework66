import React, {useState} from 'react';
import axiosApi from "../../axiosApi";
import Spinner from "../../components/UI/Spinner/Spinner";
import QuoteForm from "../../components/QuoteForm/QuoteForm";

const AddPage = ({history}) => {
    const [loading, setLoading] = useState(false);

    const [quote, setQuote] = useState({
        text: '',
        author: '',
        category: ''
    });

    const createNewQuote = async e => {
        e.preventDefault();
        setLoading(true);
        try {
            await axiosApi.post('quotes.json', quote);
        } finally {
            setLoading(false);
            history.replace('/');
        }
    }

    const onInputChange = e => {
        const {name, value} = e.target;

        setQuote(prev => ({
            ...prev,
            [name]: value
        }))
    };

    let form = (
        <QuoteForm quote={quote} onInputChange={onInputChange} createNewQuote={e => createNewQuote(e)}/>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="AddPage">
            <h1>Submit new quote</h1>
            {form}
        </div>
    );
};

export default AddPage;
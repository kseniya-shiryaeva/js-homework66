import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import Spinner from "../../components/UI/Spinner/Spinner";
import QuoteForm from "../../components/QuoteForm/QuoteForm";

const EditPage = ({history, match}) => {
    const [loading, setLoading] = useState(false);

    const [quote, setQuote] = useState({
        text: '',
        author: '',
        category: ''
    });

    useEffect(() => {
        const getQuote = async () => {
            const request = await axiosApi('/quotes/' + match.params.id + '.json');
            setQuote(request.data);
        };
        getQuote();
    }, [match.params.id]);


    const editQuote = async e => {
        e.preventDefault();
        setLoading(true);
        try {
            await axiosApi.put('quotes/' + match.params.id + '.json', quote);
        } finally {
            setLoading(false);
            history.replace('/');
        }
    };

    const onInputChange = e => {
        const {name, value} = e.target;

        setQuote(prev => ({
            ...prev,
            [name]: value
        }))
    };

    let form = (
        <QuoteForm quote={quote} onInputChange={onInputChange} createNewQuote={editQuote}/>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="EditPage">
            <h1>Edit quote</h1>
            {form}
        </div>
    );
};

export default EditPage;
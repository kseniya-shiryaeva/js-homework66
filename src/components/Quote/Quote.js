import React from 'react';
import {NavLink} from "react-router-dom";
import './Quote.css';

const Quote = ({text, author, id, removeQuote}) => {
    const editUrl = "/quotes/" + id + "/edit";

    return (
        <div className="Quote">
            <p className="text">"{text}"</p>
            <p className="author"> - {author}</p>
            <NavLink to={editUrl} className="actionButton">Edit</NavLink>
            <button onClick={removeQuote} className="actionButton">Remove</button>
        </div>
    );
};

export default Quote;
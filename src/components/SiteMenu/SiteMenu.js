import React from 'react';
import './SiteMenu.css';
import {NavLink} from "react-router-dom";

const SiteMenu = () => {
    return (
        <div className="SiteMenu">
            <NavLink to="/">Quotes</NavLink>
            <NavLink to="/add-quote">Submit new quotes</NavLink>
        </div>
    );
};

export default SiteMenu;
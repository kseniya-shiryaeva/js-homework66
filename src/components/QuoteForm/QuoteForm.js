import React from 'react';
import './QuoteForm.css';
import {CATEGORIES} from "../../constants";

const QuoteForm = ({quote, onInputChange, createNewQuote}) => {
    return (
        <form onSubmit={createNewQuote} className="QuoteForm">
            <label htmlFor="category">Category</label>
            <select name="category" id="category" onChange={onInputChange}>
                {CATEGORIES.map(category => {
                    return <option key={category.id} value={category.id}>{category.title}</option>
                })}
            </select>
            <label htmlFor="title">Author</label>
            <input
                className="Input"
                type="text"
                name="author"
                placeholder="Quote author"
                value={quote.author}
                onChange={onInputChange}
            />
            <label htmlFor="text">Quote text</label>
            <textarea
                className="Textarea"
                name="text"
                placeholder="Quote text"
                value={quote.text}
                onChange={onInputChange}>{quote.text}</textarea>
            <button type="submit">SAVE</button>
        </form>
    );
};

export default QuoteForm;
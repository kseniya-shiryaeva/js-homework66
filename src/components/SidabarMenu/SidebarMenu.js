import React from 'react';
import {NavLink} from "react-router-dom";
import {CATEGORIES} from "../../constants";

const SidebarMenu = () => {
    return (
        <div className="SidebarMenu">
            <h3>Categories</h3>
            <ul>
                <li><NavLink to="/">All</NavLink></li>
                {CATEGORIES.map(category => {
                    return <li key={category.id}><NavLink to={"/quotes/" + category.id}>{category.title}</NavLink></li>
                })}
            </ul>
        </div>
    );
};

export default SidebarMenu;
import React from 'react';
import './Layout.css';
import Header from "../../Header/Header";

const Layout = ({children}) => {
    return (
        <>
            <Header />
            <main className="Layout-Content">
                {children}
            </main>
        </>
    );
};

export default Layout;
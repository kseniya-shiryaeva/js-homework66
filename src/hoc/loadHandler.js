import React, {useEffect, useMemo, useState} from 'react';
import Spinner from "../components/UI/Spinner/Spinner";

const loadHandler = (WrappedComponent, axios) => {
    return function LoadHandlerHOC (props) {
        const [showLoader, setShowLoader] = useState(null);

        const icId = useMemo(() => {
            return axios.interceptors.request.use(request => {
                setShowLoader(true);
                return request;
            })
        }, []);

        const ic = useMemo(() => {
            return axios.interceptors.response.use(response => {
                setShowLoader(false);
                return response;
            })
        }, []);

        useEffect(() => {
            return () => {
                axios.interceptors.response.eject(icId);
            }
        }, [icId]);

        useEffect(() => {
            return () => {
                axios.interceptors.response.eject(ic);
            }
        }, [ic]);

        const dismiss = () => {
            setShowLoader(null);
        }

        let loader = null;

        if (showLoader) {
            loader = <Spinner show={showLoader} />;
        }

        return (
            <>
                {loader}
                <WrappedComponent {...props} />
            </>
        );
    };
};

export default loadHandler;
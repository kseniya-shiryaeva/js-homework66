import {Route, Switch} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import AddPage from "./containers/AddPage/AddPage";
import EditPage from "./containers/EditPage/EditPage";
import Layout from "./components/UI/Layout/Layout";

function App() {
  return (
      <Layout>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/quotes/:category" exact component={HomePage} />
          <Route path="/add-quote" component={AddPage} />
          <Route path="/quotes/:id/edit" component={EditPage} />
          <Route render={()=><h1>404: Page is not found</h1>} />
        </Switch>
      </Layout>
  );
}

export default App;
